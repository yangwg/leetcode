# Complexity Of BackTracking

# General

- [Backtracking-Summary](https://medium.com/algorithms-and-leetcode/backtracking-e001561b9f28)

### [How to calculate time complexity of backtracking algorithm?](https://stackoverflow.com/questions/20049829/how-to-calculate-time-complexity-of-backtracking-algorithm)

1. Hamiltonian cycle : `O(N!)` in the worst case
2. WordBreak and StringSegment : `O(2^N)`
3. NQueens : `O(N!)`

Note: For WordBreak there is an O(N^2) dynamic programming solution.

1. In Hamiltonian cycle, in each recursive call one of the remaining vertices is selected in the worst case. In each recursive call the branch factor decreases by 1. Recursion in this case can be thought of as n nested loops where in each loop the number of iterations decreases by one. Hence the time complexity is given by:

   `T(N) = N*(T(N-1) + O(1))`
   `T(N) = N*(N-1)*(N-2).. = O(N!)`

2. Similarly in NQueens, each time the branching factor decreases by 1 or more, but not much, hence the upper bound of `O(N!)`


# Special Question 

## [Combinations](https://leetcode.com/problems/combinations/description/)

## Combination Sum

## Permutations

## 47_Permutations II

> leetcode.com/problems/permutations-ii/discuss/18594/Really-easy-Java-solution-much-easier-than-the-solutions-with-very-high-vote/121098

The worst-case time complexity is O(n! * n).

1. For any recursive function, the time complexity is O(branches^depth) * amount of work at each node in the recursive call tree. However, in this case, we have n*(n-1)*(n*2)*(n-3)*...*1 branches at each level = n!, so the total recursive calls is O(n!)
2. We do n-amount of work in each node of the recursive call tree, (a) the for-loop and (b) at each leaf when we add n elements to an ArrayList. So this is a total of O(n) additional work per node.

Therefore, the upper-bound time complexity is O(n! * n).

We can do some optimizations on the above code:

1. Only considering unique elements
2. Add each unique element (as a key) and its frequency (as the value) to a Hash Table
3. Iterate through each key of the Hash Table, and decrease the quantity of each element we use in each recursive call

Advantages of the optimized approach:

1. Reduce the size of the for-loop -- rather than iterating through ALL of the elements of the array, we only need to iterate through the unique elements
2. Sorting step also becomes unnecessary
3. Minimize the space complexity; we don't need to allocate a huge array when we have many duplicates

Through some involved math we can probably derive a tighter upper bound for this approach, since we are doing less work at each level, e.g. n amount of work at each of the first level nodes, n-1 amount of work at each of the second level nodes, etc, but for our (interview) purposes the best upper bound is still the same, **equal to O(n! * n).**

## [SubSet](https://leetcode.com/problems/subsets/description/)

> http://www.1point3acres.com/bbs/thread-117602-1-1.html
>
> 刚好在某本书上碰到这两道题，subset这道题的答案的解释是：
>
> The number of recursive calls, T(n) satisfies the recurrence T(n) = T(n - 1) + T(n - 2) + ... + T(1) + T(0), which solves to T(n) = O(2^n). Since we spend O(n) time within a call, the time complexity is O(n2^n);

> https://www.jiuzhang.com/qa/1601/
>
> 因为是大O，且解的平均长度是n/2，所以扩2倍也没有关系，可以认为是n的。 实际复杂度是O(2^n * n / 2) = O(2^(n-1) * n)
>
> 严格意义上来讲是
>
> 1 * 长度为1的解的个数 + 2 * 长度为2的解的个数 + 3 * 长度为3的解的个数 + n * 长度为n的解的个数 =
> 1 * c(n, 1) + 2 * c(n, 2) + ..... + n * c(n,n)
> // c(m,n) 是排列组合中的组合
>
> 又因为
> i * C(n, i) = i * n! / [i! (n-i)!] = n! / [(i-1)! (n-i)!] = n * (n-1)! / [(i-1)! (n-i)!] = n * C(n-1,i-1)
>
> 所以
> 1 * c(n, 1) + 2 * c(n, 2) + ..... + n * c(n,n) = sum(i * c(n, i)) = sum(n * c(n - 1, i - 1)) = n * sum(c(n-1, i-1)) 其中 1 <= i <=n
>
> sum(c(n - 1, i - 1)) 就是杨辉三角的某一层的求和 ，为2^(n - 1)
>
> 最后得出：1 * c(n, 1) + 2 * c(n, 2) + ..... + n * c(n,n) = 2^(n - 1) * n
>
> 跟我们的估算完全一致。有时候不需要数学推导，因为是大O，又是搜索算法，故用我们的公式估计一个大概就可以了。

## Palindrome Partioning

