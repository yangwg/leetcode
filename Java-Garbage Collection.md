# Garbage Collection

## 1-Mark and Sweep Garbage Collection

A mark-and-sweep garbage collector runs in two separate phases.

The _mark_ phase does a **DFS from every root**, and **marks all the live objects**. The _sweep_ phase does a pass over all the objects in memory. Each object that was **not marked as being live is garbage, and its memory is reclaimed.**

How does the sweep phase do a pass over all the objects in memory? The JVM has an elaborate internal data structure for managing the heap. This hidden data structure keeps track of free memory and allocated memory so that new objects can be allocated without overwriting live ones. Time prevents my describing Java’s heap data structure here, but it allows the garbage collector to do a pass over every object, even the ones that are not live. It’s roughly like an invisible linked list that links _everything_.

Similarly, the stack frames on the stack are data structures that make it possible for the garbage collector to determine which data on the stack are references, and which are not.

When a mark-and-sweep collector runs, your program stops running for an instant while the garbage collector does a mark pass and a sweep pass. **The garbage collector is typically started when the JVM tries to create a new object but doesn’t have enough memory for it.**

### Compacting

Typical programs allocate and forget a good many objects. One problem that arises is _fragmentation_, the tendency of the free memory to get broken up into lots of small pieces. Fragmentation can render Java unable to allocate a large object despite having lots of free memory available.

(Fragmentation also means that the memory caches and virtual memory don’t perform as well. If you don’t know why, wait until CS 61C or CS 152.)

To solve this problem, a compacting garbage collector actually picks up the objects and moves them to different locations in memory, thereby removing the space between the objects. This is easily done during the sweep phase.

### References

There’s a problem here: if we pick up an object and move it, what about all the references to that object? Aren’t those references wrong now?

Interestingly, in the Oracle JVM, a reference isn’t a pointer. A reference is a handle. A _handle_ is a pointer to a pointer.

When an object moves, Java corrects the second pointer so it points to the object’s new address. That way, even if there are a million references to the object, they’re all corrected in one fell swoop. The "second pointers" are kept in a special table, since they don’t take as much memory as objects.

The special table of "second pointers" does not suffer from fragmentation **because all pointers have exactly the same size**. Objects suffer from fragmentation because when a small object is garbage collected, the space it leaves behind might not be large enough to accommodate a larger object. But a garbage-collected object’s "second pointer" can simply be reused by any newly constructed object that comes along, **because all "second pointers" have the same size.**

# 2-Copying Garbage Collection

Copying garbage collection is an alternative to mark and sweep. It does compaction, but it is f**aster than mark and sweep with compaction** because there is **only one phase**, rather than a mark phase and a sweep phase.

Memory is divided into two distinct spaces, called **the old space and the new space**. A **copying garbage collector** finds the live objects by **DFS** as usual, but when it encounters an object in the old space, **it _immediately_ moves it to the new space**. The object is moved to the first available memory location in the new space, so **compaction is part of the deal.** After all the objects are moved to the new space, the garbage objects that remain in the old space are simply forgotten. **There is no need for a sweep phase**.

Next time the garbage collector runs, the new space is relabeled the "old space" and the old space is relabeled the "new space". Long-lived objects may be copied back and forth between the two spaces many times.

While your program is running (between garbage collections), all your objects are in one space, while the other space sits empty.

The advantage of copying garbage collection is that **it’s fast**. The disadvantage is that you effectively **cut in half the amount of heap memory availabl**e to your program.

### 2.1-Generational Garbage Collection

Studies of memory allocation have shown that most objects allocated by most programs have short lifetimes, while a few go on to survive through many garbage collections. This observation has inspired generational garbage collectors, which **separate old from new objects.**

A generational collector has two or more generations, which are like the separate spaces used by copying collectors, except that the generations can be of different sizes, and can change size during a program’s lifetime.

Sun’s 1.3 JVM divides objects into an **old generation** and a **young generation**. Because old objects tend to **last longer**, the old generation doesn’t need to be garbage collected nearly as often. Hence, the old generation uses a **compacting mark-and-sweep collector**, because **speed** is not critical, but **memory efficiency** might be. Because old objects are long-lived, and because mark and sweep only uses one memory space, the old generation tends to remain compact.

The young generation is itself divided into three areas. The largest area is called "Eden", and it is the space where all objects are born, and most die. Eden is large enough that most objects in it will become garbage long before it gets full. **When Eden fills up**, it is garbage collected and the surviving objects are copied into one of **two _survivor_spaces_**. The survivor spaces are just the two spaces of a **copying garbage collector**.

If an unexpectedly large number of objects survive Eden, the survivor spaces can expand if necessary to make room for additional objects.

Objects **move back and forth** between the two survivor spaces until they age enough to be **_tenured_** - moved to the **old generation**. Young objects benefit from the speed of the copying collector while they’re still wild and prone to die young.

Thus, the Sun JVM takes advantage of the best features of both the mark-and-sweep and copying garbage collection methods.

There are two types of garbage collection: **minor collections**, which happen frequently but only affect the young generation - thereby saving lots of time -and **major collections**, which happen much less often but cover all the objects in memory.

This introduces a problem. Suppose a young object is live only because an old object references it. How does the minor collection find this out, if it doesn’t search the old generation?

References from old objects to young objects tend to be rare, because old objects are set in their ways and don’t change much. Since references from old objects to young are so rare, the JVM keeps a special table of them, which it updates whenever such a reference is created. The table of references is added to the roots of the young generation’s copying collector.

![Java Garbage Collection Heaps](http://stackify.com/wp-content/uploads/2017/05/Java-Garbage-Collection.png)

*Image via*[*Wikipedia*](https://de.wikipedia.org/wiki/Datei:JavaGCgenerations.png)

------

举个例子抛砖引玉，下面这个问题是我以前常问的，从应届生到工作十几年的人都问过：

> 引用
>
> “地球人都知道，Java有个东西叫垃圾收集器，它让创建的对象不需要像c/cpp那样delete、free掉，你能不能谈谈，GC是在什么时候，对什么东西，做了什么事情？”

## 1-什么时候

我自己分析一下这个问题，首先是“什么时候”，不同层次的回答从低到高排列：

1.系统空闲的时候。

分析：这种回答大约占30%，遇到的话一般我就会准备转向别的话题，譬如算法、譬如SSH看看能否发掘一些他擅长的其他方面。

2.系统自身决定，不可预测的时间/调用System.gc()的时候。

分析：这种回答大约占55%，大部分应届生都能回答到这个答案，起码不能算错误是吧，后续应当细分一下到底是语言表述导致答案太笼统，还是本身就只有这样一个模糊的认识。

3.能说出新生代、老年代结构，能提出minor gc/full gc

分析：到了这个层次，基本上能说对GC运作有概念上的了解，譬如看过《深入JVM虚拟机》之类的。这部分不足10%。

4.能说明minor gc/full gc的触发条件、OOM的触发条件，降低GC的调优的策略。

分析：列举一些我期望的回答：**eden满了minor gc，升到老年代的对象大于老年代剩余空间full gc**，或者小于时被HandlePromotionFailure参数强制full gc；gc与非gc时间耗时超过了GCTimeRatio的限制引发OOM，调优诸如通过NewRatio控制新生代老年代比例，通过MaxTenuringThreshold控制进入老年前生存次数等……能回答道这个阶段就会给我带来比较高的期望了，当然面试的时候正常人都不会记得每个参数的拼写，我自己写这段话的时候也是翻过手册的。回答道这部分的小于2%。

PS：加起来不到100%，是因为有确实少数直接说不知道，或者直接拒绝回答的= =#

## 2-什么东西

分析第二个问题：“对什么东西”：

1.不使用的对象。

分析：相当于没有回答，问题就是在问什么对象才是“不使用的对象”。大约占30%。

2.超出作用域的对象/引用计数为空的对象。

分析：这2个回答站了60%，相当高的比例，估计学校教java的时候老师就是这样教的。第一个回答没有解决我的疑问，gc到底怎么判断哪些对象在不在作用域的？至于引用计数来判断对象是否可收集的，我可以会补充一个下面这个例子让面试者分析一下obj1、obj2是否会被GC掉？

```
 class C{ 
    public Object x; 
 } 

 C obj1、obj2 = new C(); 
 obj1.x = obj2; 
 obj2.x = obj1; 
 obj1、obj2 = null;
```

3.从gc root开始搜索，搜索不到的对象。

分析：根对象查找、标记已经算是不错了，小于5%的人可以回答道这步，估计是引用计数的方式太“深入民心”了。基本可以得到这个问题全部分数。

PS：有面试者在这个问补充强引用、弱引用、软引用、幻影引用区别等，不是我想问的答案，但可以加分。

4.**从root搜索不到，而且经过第一次标记、清理后，仍然没有复活的对象。**

分析：我期待的答案。但是的确很少面试者会回答到这一点，所以在我心中回答道第3点我就给全部分数。

## 3-做什么事情

最后由一个问题：“做什么事情”，这个问发挥的空间就太大了，不同年代、不同收集器的动作非常多。

1.删除不使用的对象，腾出内存空间。

分析：同问题2第一点。40%。

2.补充一些**诸如停止其他线程执行、运行finalize等的说明**。

分析：起码把问题具体化了一些，如果像答案1那样我很难在回答中找到话题继续展开，大约占40%的人。

补充一点题外话，面试时我最怕遇到的回答就是“这个问题我说不上来，但是遇到的时候我上网搜一下能做出来”。做程序开发确实不是去锻炼茴香豆的“茴”有几种写法，不死记硬背我同意，我不会纠语法、单词，但是多少你说个思路呀，要直接回答一个上网搜，我完全没办法从中获取可以评价应聘者的信息，也很难从回答中继续发掘话题展开讨论。建议大家尽量回答引向自己熟悉的，可讨论的领域，展现给面试官最擅长的一面。

3.**能说出诸如新生代做的是复制清理、from survivor、to survivor是干啥用的、老年代做的是标记清理、标记清理后碎片要不要整理、复制清理和标记清理有有什么优劣势等。**

分析：也是看过《深入JVM虚拟机》的基本都能回答道这个程度，其实到这个程度我已经比较期待了。同样小于10%。

4.除了3外，还能讲清楚串行、并行（整理/不整理碎片）、CMS等搜集器可作用的年代、特点、优劣势，并且能说明控制/调整收集器选择的方式。

分析：同上面2个问题的第四点。

最后介绍一下自己的背景，在一间不大不小的上市软件公司担任平台架构师，有3年左右的面试官经验，工作主要方向是大规模企业级应用，参与过若干个亿元级的项目的底层架构工作。

