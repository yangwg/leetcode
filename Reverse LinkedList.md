# Reverse Linked List

Base
- https://leetcode.com/problems/reverse-linked-list/discuss/58130/C++-Iterative-and-Recursive
- https://leetcode.com/problems/reverse-linked-list/discuss/58125/In-place-iterative-and-recursive-Java-solution

## 206. Reverse Linked List

### Iterative-Without Dummy

 Cur 之前的都已经reverse好了

```java
class Solution {
    public ListNode reverseList(ListNode head) {
        ListNode new_head = null;
        ListNode cur = head;
        while(cur != null){
            ListNode next = cur.next;
            cur.next = new_head;
            new_head = cur;
            cur = next;
        }
        return new_head;
    }
}
```

### Iterative-With Dummy

Cur 是reverse部分的最后一个元素

```java
class Solution {
    public ListNode reverseList(ListNode head) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode cur = head;
        while(cur != null && cur.next != null){
            ListNode next = cur.next;
            cur.next = next.next;
            next.next = dummy.next;
            dummy.next = next;
        }
        return dummy.next;
    }
}
```

### Recursive 

- Reverse part after current.

- head.next become tail,

- append head to tail


```java
class Solution {
    public ListNode reverseList(ListNode head) {
        if(head == null || head.next == null){
            return head;
        }
        else{
            ListNode new_head = reverseList(head.next);
            head.next.next = head;
            head.next = null;
            return new_head;
        }
    }
```

