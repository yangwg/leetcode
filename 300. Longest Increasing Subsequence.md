```java
class Solution {
    public int lengthOfLIS(int[] nums) {
        if(nums == null || nums.length == 0){
            return 0;
        }
        int n = nums.length;
        int[] tail = new int[n + 1];
        int tail_idx = 0;
        tail[0] = nums[0];
        for(int i = 1; i < n; i++){
            if(nums[i] > tail[tail_idx]){
                tail_idx += 1;
                tail[tail_idx] = nums[i];
            }
            else{
                int left = 0;
                int right = tail_idx;
                while(left < right){
                    int mid = left + (right - left) / 2;
                    if(nums[i] > tail[mid]){
                        left += 1;
                    }
                    else{
                        right = mid;
                    }
                }
                tail[left] = nums[i];
            }
        }
        return tail_idx + 1;
    }
}
```