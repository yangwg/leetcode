```java
class Solution {
    public double[] calcEquation(String[][] equations, double[] values, String[][] queries) {
        Map<String, Map<String, Double>> map = new HashMap<>();
        for(int i = 0; i < equations.length; i++){
            String a = equations[i][0];
            String b = equations[i][1];
            double r_ab = values[i];
            double r_ba = 1.0 / values[i];
            if(!map.containsKey(a)){
                map.put(a, new HashMap<>());
            }
            if(!map.containsKey(b)){
                map.put(b, new HashMap<>());
            }
            map.get(a).put(b, r_ab);
            map.get(b).put(a, r_ba);
        }
        double[] res = new double[queries.length];
        for(int i = 0; i < queries.length; i++){
            res[i] = dfs(queries[i][0], queries[i][1], 1.0, map, new HashSet<>());
        }
        return res;
    }
    
    private double dfs(String s, String t, double val, Map<String, Map<String, Double>> map, Set<String> set){
        // if(!map.containsKey(s) || !set.add(s)){
        if(!map.containsKey(s)){
            return -1.0;
        }
        if(s.equals(t)){
            return val;
        }
        Map<String, Double> sub_map = map.get(s);
        for(String next : sub_map.keySet()){
            if(set.contains(next)){
                continue;
            }
            set.add(next);
            double res = dfs(next, t, val * sub_map.get(next), map, set);
            set.remove(next);
            if(res != -1.0){
                return res;
            }
        }
        return -1.0;
    }
}
```