## 
```java
class Solution {
    public int[] searchRange(int[] nums, int target) {
        if(nums == null || nums.length < 1){
            return new int[]{-1, -1};
        }
        int left = find_lo(nums, target);
        int right = find_hi(nums, target);
        return new int[]{left, right};
    }
    
    private int find_lo(int[] nums, int val){
        int left = 0;
        int right = nums.length - 1;
        while(left < right){
            int mid = left + (right - left) / 2;
            if(nums[mid] >= val){
                right = mid;
            }
            else if(nums[mid] < val){
                left = mid + 1;
            }
        }
        if(nums[left] == val){
            return left;
        }
        else{
            return -1;
        }

    }
    private int find_hi(int[] nums, int val){
        int left = 0;
        int right = nums.length - 1;
        while(left < right){
            int mid = right - (right - left) / 2;
            if(nums[mid] <= val){
                left = mid;
            }
            else{
                right = mid - 1;
            }
        }
        if(nums[left] == val){
            return right;
        }
        else{
            return -1;
        }
    }
}
```