```java
/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */
class Solution {
    public List<Interval> merge(List<Interval> intervals) {
        if(intervals == null || intervals.size() < 1){
            return intervals;
        }
        //sort by start val, asc
        PriorityQueue<Interval> pQueue = new PriorityQueue<>((a, b)->(a.start - b.start));
        List<Interval> ans = new ArrayList<>();
        for(Interval i : intervals){
            pQueue.offer(i);
        }
        Interval cur = pQueue.poll();
        while(!pQueue.isEmpty()){
            Interval head = pQueue.poll();
            if(head.start > cur.end){
                ans.add(cur);
                cur = head;
            }
            else{
                cur.end = Math.max(head.end, cur.end);
            }
        }
        ans.add(cur);
        return ans;
    }
}
```