## Google - find isomorphic string的变种

<https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=443299&extra=&page=1>

给两个string src和dst，返回能否通过不限次数的转换把src变成dst-google 1point3acres 规则： 每次转换将src里的所有同一个字符转换成另一个字符，比如: abaca -> ebece, 将abacd里的a转换成e即可得到dst 另一种情况是abba -> baab 这个时候可以先把a换成c（cbbc），然后b换成a （caac），最后c换成b （baab）

> Example return false:
>
> - bb !-> bbb (different length) 来源一亩.三分地论坛.
> - cccb !-> abcb abccaa (same index, same char in src, different char in dst)
> - aabc !-> adbc (number of different characters cannot increase). From 1point 3acres bbs
> - aabbccddeegg...zz !-> bbaaccddeegg...zz (if two chars are different in dst, then we must garrantee the values of same indexes in src are different)

## MyCode

```java
package com.company;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

class Solution {
    boolean check(String a, String b){
        int num = 5;
        if(a.length() != b.length()){
            return false;
        }
        Map<Character, Character> map = new HashMap<>();
        for(int i = 0; i < a.length(); i++){
            char c_a = a.charAt(i);
            char c_b = b.charAt(i);
            if(map.containsKey(c_a)){
                if(map.get(c_a) != c_b){
                    return false;
                }
            }
            else{
                map.put(c_a, c_b);
            }
        }
        Set<Character> set_a = new HashSet<>();
        Set<Character> set_b = new HashSet<>();
        int cnt = 0;
        for(int i = 0; i < a.length(); i++){
            char c_a = a.charAt(i);
            char c_b = b.charAt(i);
            set_a.add(c_a);
            set_b.add(c_b);
            if(map.get(c_a) != c_a){
                cnt += 1;
            }
        }
        if(set_a.size() == num && set_b.size() == num  && cnt > 0){
            return false;
        }
        else{
            return true;
        }
    }
}

public class Main {

    public static void main(String[] args) {
	// write your code here
        String[] a = new String[]{"abba", "bb", "cccb", "aabc", "abcde"};
        String[] b = new String[]{"bbbb", "bbb", "abcb", "adbc", "cbade"};
        Solution s = new Solution();
        for(int i = 0; i < a.length; i++){
            System.out.println(s.check(a[i], b[i]) + " A: " + a[i] + ", B: " + b[i]);
        }
        //trueA: abba, B: bbbb
//        falseA: bb, B: bbb
//        falseA: cccb, B: abcb
//        falseA: aabc, B: adbc
//        falseA: abcde, B: cbade
    }
}

```

## Ref Code

```C++
三个判据：
1. 两个字符串大小不一样，返回false
2. 从a->b的单向映射Map出现冲突，返回false. 围观我们@1point 3 acres
3.两个字符串都有26个字符，且Map不冲突，但是存在Map[x]!=x，说明必定有成环的映射关系．这种情况下，因为无法借助第三变量（26个字母都用完了），所以返回false
其他情况都返回true

bool check(string a, string b)
{
   if (a.size()!=b.size()) return false;
    
   unordered_map<char,char>Map;
   for (int i=0; i<a.size(); i++)
   {
       if (Map.find(a[i])==Map.end())
           Map[a[i]]=b[i];
       else if (Map[a[i]]!=b[i])
           return false;
   }
    
   unordered_set<char>SetA;
   unordered_set<char>SetB;
   int count = 0;
   for (int i=0; i<a.size(); i++)
   {
       SetA.insert(a[i]);
       SetB.insert(b[i]);
       if (Map[a[i]]!=a[i]) count++;
   }
   if (SetA.size()==26 && SetB.size()==26 && count>0)
       return false;
   else
       return true;
}
```

