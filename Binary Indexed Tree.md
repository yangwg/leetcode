# Binary Index Tree

## Tutorial 1 - Very Good

### **When to use Binary Indexed Tree?**

Before going for Binary Indexed tree to perform operations over range, one must confirm that the operation or the function is:

*Associative*. i.e f(f(a,b),c)=f(a,f(b,c)) this is true even for seg-tree

*Has an inverse*. eg:

1. Addition has inverse subtraction (this example we have discussed)
2. Multiplication has inverse division
3. gcd() has no inverse, so we can’t use BIT to calculate range gcd’s
4. Sum of matrices has inverse
5. Product of matrices would have inverse if it is given that matrices are degenerate i.e. determinant of any matrix is not equal to 0

### Implementation Notes

- User array of length `n + 1`
- Start with zero-array, insert each element with `ADD`
- `RANGESUM(from, to) = SUM(to) - SUM(from - 1)`
- Also useful for other operation linke `product`, `xor`..
- But not for `min/max`

### Complexity 

- **Space Complexity**: **O(N)** for declaring another array of size N

- **Time Complexity**: **O(logN)** for each operation(update and query as well)

> Because the query climbs the two trees by following the parent link and because the height of a binomial tree with 2 K nodes is K, the time complexity of the query operation for a subarray is O (log N ) where N is the size of the subarray.

### Turorial 

- [Fenwick (Binary Indexed) Trees](https://www.hackerearth.com/zh/practice/data-structures/advanced-data-structures/fenwick-binary-indexed-trees/tutorial/)

- [Video Tutorial: Binary Indexed Tree (Fenwick Tree)](https://www.youtube.com/watch?v=v_wj_mOAlig)

- https://www.geeksforgeeks.org/binary-indexed-tree-or-fenwick-tree-2/

  https://www.topcoder.com/community/competitive-programming/tutorials/binary-indexed-trees/

## Code

> https://github.com/jakobkogler/Algorithm-DataStructures/blob/master/RangeQuery/BinaryIndexedTree.py

```python
class BIT:
    """Implementation of a Binary Indexed Tree (Fennwick Tree)"""
    
    #def __init__(self, list):
    #    """Initialize BIT with list in O(n*log(n))"""
    #    self.array = [0] * (len(list) + 1)
    #    for idx, val in enumerate(list):
    #        self.update(idx, val)

    def __init__(self, list):
        """"Initialize BIT with list in O(n)"""
        self.array = [0] + list
        for idx in range(1, len(self.array)):
            idx2 = idx + (idx & -idx)
            if idx2 < len(self.array):
                self.array[idx2] += self.array[idx]

    def prefix_query(self, idx):
        """Computes prefix sum of up to including the idx-th element"""
        idx += 1
        result = 0
        while idx:
            result += self.array[idx]
            idx -= idx & -idx
        return result

    def range_query(self, from_idx, to_idx):
        """Computes the range sum between two indices (both inclusive)"""
        return self.prefix_query(to_idx) - self.prefix_query(from_idx - 1)

    def update(self, idx, add):
        """Add a value to the idx-th element"""
        idx += 1
        while idx < len(self.array):
            self.array[idx] += add
            idx += idx & -idx


if __name__ == '__main__':
    array = [1, 7, 3, 0, 5, 8, 3, 2, 6, 2, 1, 1, 4, 5]
    bit = BIT(array)
    print('Array: [{}]'.format(', '.join(map(str, array))))
    print()

    print('Prefix sum of first {} elements: {}'.format(13, bit.prefix_query(12)))
    print('Prefix sum of first {} elements: {}'.format(7, bit.prefix_query(6)))
    print('Range sum from pos {} to pos {}: {}'.format(1, 5, bit.range_query(1, 5)))
    print()
    
    bit.update(4, 2)
    print('Add {} to element at pos {}'.format(2, 4))
    new_array = [bit.range_query(idx, idx) for idx in range(len(array))]
    print('Array: [{}]'.format(', '.join(map(str, new_array))))
    print()

    print('Prefix sum of first {} elements: {}'.format(13, bit.prefix_query(12)))
    print('Prefix sum of first {} elements: {}'.format(7, bit.prefix_query(6)))
    print('Range sum from pos {} to pos {}: {}'.format(1, 5, bit.range_query(1, 5)))
    print()
```

## Chinese Version

> https://blog.csdn.net/Yaokai_AssultMaster/article/details/79492190
>
> https://drive.google.com/file/d/1KsoYyxe8PKy0Nc4nVx9ghnEDNd8gc-qf/view?usp=sharing

## Other



https://blog.csdn.net/Yaokai_AssultMaster/article/details/79492190

https://www.youtube.com/watch?v=CWDQJGaN1gY&t=2s

https://www.youtube.com/watch?v=v_wj_mOAlig

https://www.hackerearth.com/zh/practice/data-structures/advanced-data-structures/fenwick-binary-indexed-trees/tutorial/

