> https://www.quora.com/How-can-I-solve-my-coin-change-problem-with-a-limited-number-of-coins
```c++
#include <bits/stdc++.h>
#define SIZE 55
#define MOD 100000007
#define mem(n,m) memset(n,m,sizeof(n))
using namespace std;
int T;
int N;
int A[SIZE];
int C[SIZE];
int K;
int dp[SIZE][1010];
int main()
{
    cin>>T;
    for(int tt=1;tt<=T;tt++){
        cin>>N>>K;
        for(int i=1;i<=N;i++){
            cin>>A[i];
        }
        for(int i=1;i<=N;i++){
            cin>>C[i];
        }
        mem(dp,0);
        dp[0][0] = 1;
        for(int i=1;i<=N;i++){
            for(int j=0;j<=K;j++){
                int rem = min(C[i],j/A[i]);
                for(int k=0;k<=rem;k++){
                    dp[i][j] = (dp[i][j] + dp[i-1][j-A[i]*k])%MOD;
                }
            }
        }
        cout<<"Case "<<tt<<": ";
        cout<<dp[N][K]<<endl;
    }
    return 0;
}
```