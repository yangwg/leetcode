# https://stackoverflow.com/questions/3277503/in-python-how-do-i-read-a-file-line-by-line-into-a-list


lines = []
with open("a.txt") as file:
    for l in file:
        lines.append(l)
        print(l, end='')
print()
print(len(lines))

# using compile
import re
# s = '010-12345 010-12346 010-12347'
# pattern = re.compile(r'\d{3}\-\d{3,8}')
s1 = 'aaaaaa'
pattern1 = re.compile(r'a{1,2}')
print(pattern1.findall(s1))

if(pattern1.match(s1)):
    print("Pattern Matched")
else:
    print("Not Match")
# ['aa', 'aa', 'aa']
# Pattern Matched

s2 = '010-12345 010-12346 010-12347'
pattern2 = re.compile(r'\d{3}\-\d{3,8}')
print(pattern2.match(s2))
print(pattern2.search(s2))
print(pattern2.findall(s2))
print(type(pattern2.findall(s2)))
# <_sre.SRE_Match object; span=(0, 9), match='010-12345'>
# <_sre.SRE_Match object; span=(0, 9), match='010-12345'>
# ['010-12345', '010-12346', '010-12347']
# <class 'list'>

print(pattern2.split(s2))
['', ' ', ' ', '']
# match()方法判断是否匹配，
#   - 如果匹配成功，返回一个Match对象，
#   - 否则返回None。

print(re.findall())
re.match(r'^\d{3}\-\d{3,8}$', '010-12345')
re.split(r'\s+', 'a b   c')

# re
# match
# search
# split
# findall
# 将正则表达式编译成Pattern对象
pattern = re.compile(r'world')

# 使用search()查找匹配的子串，不存在能匹配的子串时将返回None
# 这个例子中使用match()无法成功匹配
match = pattern.search('hello world!')
if match:
    # 使用Match获得分组信息
    print
    match.group()
### 输出 ###
# world

import re
p = re.compile(r'\d+')
print p.split('one1two2three3four4')

### output ###
# ['one', 'two', 'three', 'four', '']


import re
p = re.compile(r'\d+')
print p.findall('one1two2three3four4')

### output ###
# ['1', '2', '3', '4']

text = 'geeks for geeks'
# Splits at space
print(text.split())
word = 'geeks, for, geeks'
# Splits at ','
print(word.split(', '))
word = 'geeks:for:geeks'
# Splitting at ':'
print(word.split(':'))
# ['geeks', 'for', 'geeks']
# ['geeks', 'for', 'geeks']
# ['geeks', 'for', 'geeks']