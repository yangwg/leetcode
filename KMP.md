> https://www.geeksforgeeks.org/kmp-algorithm-for-pattern-searching/
> https://www.youtube.com/watch?v=GTJr8OvyEVQ
```java
class KMP{
    void KMP_search(String pattern, String s){
        int[] lps = new int[pattern.length()];
        int i = 0;
        int j = 0;
        compute_LPS(pattern, lps);
        while(i < s.length()){
            if (pattern.charAt(j) == s.charAt(i)){
                i += 1;
                j += 1;
            }
            if(j == pattern.length()){
                System.out.println("Found Pattern at idx:" + (i - j));
                j = lps[j - 1];
            }
            else if (i < s.length() && pattern.charAt(j) != pattern.charAt(i)){
                if(j != 0){
                    j = lps[j - 1];
                }
                else{
                    i = i + 1;
                }
            }
        }
    }
    void compute_LPS(String pattern, int[] lps){
        int len = 0;
        int i = 1;
        lps[0] = 0;
        while(i < pattern.length()){
            if(pattern.charAt(i) == pattern.charAt(len)){
                len += 1;
                lps[i] = len;
                i += 1;
            }
            else{
                if(len != 0){
                    len = lps[len - 1];
                }
                else{
                    lps[i] = len;
                    i += 1;
                }
            }
        }
    }
}
```